import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy
from scipy.integrate import odeint
plt.rcParams.update({'font.size': 22})


T = 373.15
p_CO = 0.4
p_O2 = 0.2
p_CO2 = 0.4
kB = 8.617e-5
h = 4.13566e-15
k_1 = 3.8719294e6
k_2 = 5.1231558e6
k_minus_1 = k_1/np.exp(-(-0.724)/(kB*T))
k_minus_2 = k_2/np.exp(-(-1.108)/(kB*T))
k_3 = kB*T/h*np.exp(-(1.167)/(kB*T))
k_minus_3 = kB*T/h*np.exp(-(2.852)/(kB*T))


def ode_fun_CO_ox(theta,t,k_1,k_2,k_minus_1,k_minus_2,k_3,k_minus_3,p_CO,p_O2,p_CO2):
    theta_v, theta_CO, theta_O = theta
    r1 = k_1*p_CO*theta_v - k_minus_1*theta_CO
    r2 = 2*k_2*p_O2*theta_v - k_minus_2*theta_O
    r3 = k_3*theta_CO*theta_O - k_minus_3*p_CO2*theta_v
    return [-r1 -r2 +2*r3, r1 - r3, r2 - r3]

t = np.linspace(0,20000,1000)
y0 = [1.0, 0., 0.]
regular = odeint(ode_fun_CO_ox,y0,t,args=(k_1,k_2,k_minus_1,k_minus_2,k_3,k_minus_3,p_CO,p_O2,p_CO2))

print(regular[-1,:])
#def ode_fun_wrt_CO(theta,t,k_1,k_2,k_minus_1,k_minus_2,k_3,k_minus_3,p_CO,p_O2,p_CO2):
#    theta_CO, theta_O, theta_v = theta
#    return [-k_3*theta_O - k_minus_1, -k_3*theta_O, 2*k_3*theta_O + k_minus_1]

#wrt_CO = odeint(ode_fun_wrt_CO,y0,t,args=(k_1,k_2,k_minus_1,k_minus_2,k_3,k_minus_3,p_CO,p_O2,p_CO2))

######## plot classical with quantum overlay
fig,ax = plt.subplots(figsize=(8,4))
#lineObjects = ax.plot(regular,linewidth=2)
ax.tick_params(bottom=False)
ax.set_xticks([])
ax.plot(t,regular[:,0], color='tab:blue', linewidth=4, label=r'$\theta_v$')
ax.plot(t,regular[:,1], color='tab:orange', linewidth=4, label=r'$\theta_{CO}$')
ax.plot(t,regular[:,2], color='tab:green', linewidth=4, label=r'$\theta_O$')
#ax.legend(iter(lineObjects), (r'$\theta_{CO}$',r'$\theta_O$',r'$\theta_v$'))
ax.plot(18000,0, color='tab:blue', marker='o', fillstyle='full',linestyle='none', mew=16, label=r'$\theta_v$')
ax.plot(18000,0, color='tab:orange', marker='o',linestyle='none', mew=10, label=r'$\theta_{CO}$')
ax.plot(18000,1, color='tab:green', marker='o',linestyle='none', mew=10, label=r'$\theta_O$')
ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), frameon=False)
ax.set_yticks(np.arange(0, 2, step=1.0))
ax.set_xlabel(r'time')
ax.set_ylabel(r'$\theta$')
fig.tight_layout()
fig.savefig('CO_oxidation_hero_image.png',dpi=220)

