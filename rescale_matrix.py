import numpy as np

T = 373.15
p_CO = 0.04#0.4
p_O2 = 0.02#0.2
p_CO2 = 0.04#0.4
kB = 8.617e-5
h = 4.13566e-15
k_1 = 3.8719294e6
k_2 = 5.1231558e6
k_minus_1 = k_1/np.exp(-(0.276)/(kB*T)) #-0.724
k_minus_2 = k_2/np.exp(-(-0.108)/(kB*T)) #-1.108
k_3 = kB*T/h*np.exp(-(1.167)/(kB*T))
k_minus_3 = kB*T/h*np.exp(-(2.852)/(kB*T))

theta_O = 0
M_alg = np.array([[1, 1, 1, 0],
         [k_1*p_CO+k_minus_3*p_CO2, -k_minus_1-k_3*theta_O, 0, 0],
         [2*k_2*p_O2+k_minus_3*p_CO2, -k_3*theta_O, -k_minus_2, 0],
         [0, 0, 0, 1]])
M_altered = np.array([[1, 1, 1, 0],
         [-k_minus_1, k_1*p_CO+k_minus_3*p_CO2, 0, 0],
         [-k_minus_1, 0, -k_minus_2 * (k_1*p_CO+k_minus_3*p_CO2)/(2*k_2*p_O2+k_minus_3*p_CO2),0],
         [0, 0, 0, 1]])
print('M_altered',M_altered)
print('M_altered solution',np.matmul(np.linalg.inv(M_altered),np.array([[1],[0],[0],[0]])))
print('M_alg',M_alg)
theta = np.matmul(np.linalg.inv(M_alg),np.array([[1],[0],[0],[0]]))
u, v = np.linalg.eig(M_alg)
print('M_alg eigenvalues', u)
print('theta',theta)
P = np.diag(np.diag(M_alg))
print('P',P)
preconditioned_M_alg = np.matmul(np.linalg.inv(P),M_alg)
print('preconditioned matrix', preconditioned_M_alg)
u, v = np.linalg.eig(preconditioned_M_alg)
print('eigenvalues of preconditioned matrix', u)
print(max(u))
print(min(u))
kappa_squared = np.power(max(u)/min(u),2)
print('kappa squared', kappa_squared)


M_alg[1,:] = M_alg[1,:]/max(M_alg[1,:])
M_alg[2,:] = M_alg[2,:]/max(M_alg[2,:])
print(M_alg)
u, v = np.linalg.eig(M_alg)
print('eigenvalues',u)
