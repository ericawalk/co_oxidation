import numpy as np


T = 373.15
p_CO = 0.4
p_O2 = 0.2
p_CO2 = 0.4
kB = 8.617e-5
h = 4.13566e-15
k_1 = 3.8719294e6
k_2 = 5.1231558e6
k_minus_1 = k_1/np.exp(-(-0.724)/(kB*T))
k_minus_2 = k_2/np.exp(-(-1.108)/(kB*T))
k_3 = kB*T/h*np.exp(-(1.167)/(kB*T))
k_minus_3 = kB*T/h*np.exp(-(2.852)/(kB*T))

theta_O = 0
M_alg = np.array([[1, 1, 1],
         [-k_minus_1-k_3*theta_O, 0, k_1*p_CO+k_minus_3*p_CO2],
         [-k_3*theta_O, -k_minus_2, 2*k_2*p_O2+k_minus_3*p_CO2]])
M_squiggle = np.zeros((8,8))
M_squiggle[0:3,3:6] = M_alg
M_squiggle[3:6,0:3] = np.transpose(M_alg)
M_squiggle[6,6] = 1
M_squiggle[7,7] = 1
print(np.matmul(np.transpose(M_squiggle),M_squiggle))

ws, vs = np.linalg.eigh(M_squiggle)
q, r = np.linalg.qr(vs)
U = q
U[:,0] = vs[:,0]
print('U: ', U)
print(np.matmul(np.transpose(U),U))
