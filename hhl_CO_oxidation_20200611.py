"""
Harrow, Hassidim and Lloyd algorithm for a CO oxidation microkinetic model.
Code adapted from the Cirq example: https://github.com/quantumlib/Cirq/blob/master/examples/hhl.py


=== REFERENCE ===
Harrow, Aram W. et al. Quantum algorithm for solving linear systems of
equations (the HHL paper)
https://arxiv.org/abs/0811.3171

Coles, Eidenbenz et al. Quantum Algorithm Implementations for Beginners
https://arxiv.org/abs/1804.03719

"""

import math
import numpy as np
import sympy
import cirq
from scipy.linalg import solve

class PhaseEstimation(cirq.Gate):
    """
    A gate for Quantum Phase Estimation.

    unitary is the unitary gate whose phases will be estimated.
    The last qubit stores the eigenvector; all other qubits store the
    estimated phase, in big-endian.
    """

    def __init__(self, num_qubits, unitary):
        self._num_qubits = num_qubits
        self.U = unitary

    def num_qubits(self):
        return self._num_qubits

    def _decompose_(self, qubits):
        qubits = list(qubits)
        memory_size = 3# I am not sure how to pass this to this class without screwing up everything.
        yield cirq.H.on_each(*qubits[:-memory_size])
        yield PhaseKickback(self.num_qubits(), self.U)(*qubits)
        yield cirq.QFT(*qubits[:-memory_size], without_reverse=True)**-1

class encodeEigenvectors(cirq.ThreeQubitGate):
    def _unitary_(self):
        T = 373.15
        p_CO = 0.4
        p_O2 = 0.2
        p_CO2 = 0.4
        kB = 8.617e-5
        h = 4.13566e-15
        k_1 = 3.8719294e6
        k_2 = 5.1231558e6
        k_minus_1 = k_1/np.exp(-(-0.724)/(kB*T))
        k_minus_2 = k_2/np.exp(-(-1.108)/(kB*T))
        k_3 = kB*T/h*np.exp(-(1.167)/(kB*T))
        k_minus_3 = kB*T/h*np.exp(-(2.852)/(kB*T))

        theta_O = 0
        M_alg = np.array([[1, 1, 1, 0],
             [k_1*p_CO+k_minus_3*p_CO2, -k_minus_1-k_3*theta_O, 0, 0],
             [2*k_2*p_O2+k_minus_3*p_CO2, -k_3*theta_O, -k_minus_2, 0],
             [0, 0, 0, 1]])
        # x is theta_V, theta_CO, theta_O
        M_squiggle = np.zeros((8,8))
        M_squiggle[0:4,4:8] = M_alg
        M_squiggle[4:8,0:4] = np.transpose(M_alg)    
        #M_squiggle = M_squiggle*10000

        ws, vs = np.linalg.eigh(M_squiggle)
        q, r = np.linalg.qr(vs)
        U = q
        U[:,0] = vs[:,0]
        return U

class HamiltonianSimulation(cirq.EigenGate, cirq.ThreeQubitGate): #Single qubit part here removed, too.
    """
    A gate that represents e^iAt.

    This EigenGate + np.linalg.eigh() implementation is used here
    purely for demonstrative purposes. If a large matrix is used,
    the circuit should implement actual Hamiltonian simulation,
    by using the linear operators framework in Cirq for example.
    """

    def __init__(self, A, t, exponent=1.0):
        cirq.ThreeQubitGate.__init__(self) #This "single" part worries me.
        #cirq.Gate.num_qubits=3
        cirq.EigenGate.__init__(self, exponent=exponent)
        self.A = A
        self.t = t
        ws, vs = np.linalg.eigh(A)
        self.eigen_components = []
        for w, v in zip(ws, vs.T):
            theta = w*t / math.pi
            P = np.outer(v, np.conj(v))
            self.eigen_components.append((theta, P))

    def _with_exponent(self, exponent):
        return HamiltonianSimulation(self.A, self.t, exponent)

    def _eigen_components(self):
        return self.eigen_components


class PhaseKickback(cirq.Gate):
    """
    A gate for the phase kickback stage of Quantum Phase Estimation.

    It consists of a series of controlled e^iAt gates with the memory qubits as
    the target and each register qubit as the control, raised
    to the power of 2 based on the qubit index.
    unitary is the unitary gate whose phases will be estimated.
    """

    def __init__(self, num_qubits, unitary):
        super(PhaseKickback, self)
        self._num_qubits = num_qubits
        self.U = unitary

    def num_qubits(self):
        return self._num_qubits

    def _decompose_(self, qubits):
        qubits = list(qubits)
        memory_size = 3
        memory = qubits[-memory_size:] #The memory qubits are at the end of the list of qubits.
        qubits = qubits[:-memory_size] #The register qubits are before.
        for i, qubit in enumerate(qubits):
            yield cirq.ControlledGate(self.U**(2**i))(qubit, *memory)


class EigenRotation(cirq.Gate):
    """
    EigenRotation performs the set of rotation on the ancilla qubit equivalent
    to division on the memory register by each eigenvalue of the matrix. The
    last qubit is the ancilla qubit; all remaining qubits are the register,
    assumed to be big-endian.

    It consists of a controlled ancilla qubit rotation for each possible value
    that can be represented by the register. Each rotation is a Ry gate where
    the angle is calculated from the eigenvalue corresponding to the register
    value, up to a normalization factor C.
    """

    def __init__(self, num_qubits, C, t):
        super(EigenRotation, self)
        self._num_qubits = num_qubits
        self.C = C
        self.t = t
        self.N = 2**(num_qubits-1)

    def num_qubits(self):
        return self._num_qubits

    def _decompose_(self, qubits):
        for k in range(self.N):
            kGate = self._ancilla_rotation(k)

            # xor's 1 bits correspond to X gate positions.
            xor = k ^ (k-1)

            for q in qubits[-2::-1]:
                # Place X gates
                if xor % 2 == 1:
                    yield cirq.X(q)
                xor >>= 1

                # Build controlled ancilla rotation
                kGate = cirq.ControlledGate(kGate)

            yield kGate(*qubits)

    def _ancilla_rotation(self, k):
        if k == 0:
            k = self.N
        theta = 2*math.asin(self.C * self.N * self.t / (2*math.pi * k))
        return cirq.ry(theta)


def hhl_circuit(A, C, t, register_size, memory_size):
    """
    Constructs the HHL circuit.

    Args:
        A: The input Hermitian matrix.
        C: Algorithm parameter, see above.
        t: Algorithm parameter, see above.
        register_size: The size of the eigenvalue register.
        memory_basis: The basis to measure the memory in, one of 'x', 'y', 'z'.
        input_prep_gates: A list of gates to be applied to |0> to generate the
            desired input state |b>.

    Returns:
        The HHL circuit. The ancilla measurement has key 'a' and the memory
        measurement is in key 'm'.  There are two parameters in the circuit,
        `exponent` and `phase_exponent` corresponding to a possible rotation
        applied before the measurement on the memory with a
        `cirq.PhasedXPowGate`.
    """

    ancilla = cirq.LineQubit(0)
    # to store eigenvalues of the matrix
    register = [cirq.LineQubit(i + 1) for i in range(register_size)]
    # to store input and output vectors
    memory = [cirq.LineQubit(register_size + i + 1) for i in range(memory_size)]
    #print(ancilla,register,memory)

    c = cirq.Circuit()
    hs = HamiltonianSimulation(A, t)
    #print('Unitary matrix representation of the Hamiltonian Simulation circuit, before it is made to a control circuit: ', cirq.unitary(hs))
    #print(*(register + memory))
    pe = PhaseEstimation(register_size + memory_size, hs) # The "memory_size" used to be 1.  However, our phase estimation is going to do Unitary operations (from the Hamiltonian simulation) on the whole memory register, which is now more that one qubit.
    #c.append([gate(memory) for gate in input_prep_gates]) Our b vector happens to be [1, 0, 0 ...]^T  Start all the qubits at |a>
    c.append([
        encodeEigenvectors()(*memory),
        pe(*(register + memory)), # We are going to perform Unitary transormations on all memory qubits.  The phase estimation function (and the phase kickback function) assume the memory is one qubit, but we will go in and change that.
        EigenRotation(register_size + 1, C, t)(*(register + [ancilla])), #Fortunately, this requires no adjustment for the increase in the number of qubits in the memory register.
        pe(*(register + memory))**-1, 
        cirq.measure(ancilla, key='a'),
        cirq.measure(*register, key='r'),
        cirq.measure(*memory, key='m')
    ])

    #c.append([ #Actually, we just want to read out the memory.
    #    cirq.PhasedXPowGate(
    #        exponent=sympy.Symbol('exponent'),
    #        phase_exponent=sympy.Symbol('phase_exponent'))(memory),
    #    cirq.measure(memory, key='m')
    #])

    print(c) #EAW
    return c


def simulate(circuit):
    simulator = cirq.Simulator()

    # Cases for measurring X, Y, and Z (respectively) on the memory qubit.
    #params = [{
    #    'exponent': 0.5,
    #    'phase_exponent': -0.5
    #}, {
    #    'exponent': 0.5,
    #    'phase_exponent': 0
    #}, {
    #    'exponent': 0,
    #    'phase_exponent': 0
    #}]

    result = simulator.run(circuit, repetitions=5000)
    #results = simulator.run_sweep(circuit, params, repetitions=5000)

    #for label, result in zip(('X', 'Y', 'Z'), list(results)):
        # Only select cases where the ancilla is 1.
        # TODO: optimize using amplitude amplification algorithm.
        # Github issue: https://github.com/quantumlib/Cirq/issues/2216
    #    expectation = 1 - 2 * np.mean(
    #        result.measurements['m'][result.measurements['a'] == 1])
    #    print('{} = {}'.format(label, expectation))
    print(result.histogram(key='m'))
    print(result.measurements['m'])
    print(result.measurements['a'].flatten()==1)
    print(result.measurements['m'][result.measurements['a'].flatten() == 1, :])
    MyDictionary = result.histogram(key='m')
    register_dictionary = result.histogram(key='r')
    #print('register: ', register_dictionary)
    ancilla = result.histogram(key='a')
    print('ancilla: ', ancilla)
    import matplotlib
    import matplotlib.pyplot as plt
    print(dict(MyDictionary))
    fig, axs = plt.subplots(figsize=(4,4))
    myDict = dict(MyDictionary)
    plt.bar(myDict.keys(), myDict.values(), width = 1, color='g')
    fig.tight_layout()
    fig.savefig('hhl_CO_ox_memory.png',dpi=220)

def main():
    """
    Simulates HHL with matrix input, and outputs Pauli observables of the
    resulting qubit state |x>.
    Expected observables are calculated from the expected solution |x>.
    """

    T = 373.15
    p_CO = 0.4
    p_O2 = 0.2
    p_CO2 = 0.4
    kB = 8.617e-5
    h = 4.13566e-15
    k_1 = 3.8719294e6
    k_2 = 5.1231558e6
    k_minus_1 = k_1/np.exp(-(-0.724)/(kB*T))
    k_minus_2 = k_2/np.exp(-(-1.108)/(kB*T))
    k_3 = kB*T/h*np.exp(-(1.167)/(kB*T))
    k_minus_3 = kB*T/h*np.exp(-(2.852)/(kB*T))

    theta_O = 0
    M_alg = np.array([[1, 1, 1, 0],
             [k_1*p_CO+k_minus_3*p_CO2, -k_minus_1-k_3*theta_O, 0, 0],
             [2*k_2*p_O2+k_minus_3*p_CO2, -k_3*theta_O, -k_minus_2, 0],
             [0, 0, 0, 1]])
    # x is theta_V, theta_CO, theta_O
    M_squiggle = np.zeros((8,8))
    M_squiggle[0:4,4:8] = M_alg
    M_squiggle[4:8,0:4] = np.transpose(M_alg)
    #M_squiggle[6,6] = 1
    #M_squiggle[7,7] = 1
    #M_squiggle = M_squiggle*10000
    print(M_squiggle)
    # print classical solution here.  print(np.linalg.inverse(A)
    b = np.zeros((8,1))
    b[0] = 1
    b_alg = np.zeros((4,1))
    b_alg[0] = 1
    print('A inverse: ', np.linalg.inv(M_squiggle))
    print('Classical solution: ', np.matmul(np.linalg.inv(M_squiggle),b))
    print('Classical solution for original 3x3: ', np.matmul(np.linalg.inv(M_alg),b_alg))
    print(solve(M_alg,b_alg))
    A = M_squiggle

    #A = np.array([[4.30213466-6.01593490e-08j,
    #               0.23531802+9.34386156e-01j],
    #              [0.23531882-9.34388383e-01j,
    #               0.58386534+6.01593489e-08j]])
    t = 0.10*math.pi #0.358166
    register_size = 4
    memory_size = 3
    ws, vs = np.linalg.eigh(M_squiggle)
    print('eigenvalues: ', ws)
    print(np.min(np.abs(ws)))
    print('first eigenvector: ', vs[:,0])
    #input_prep_gates = [cirq.rx(1.276359), cirq.rz(1.276359)]

    # Set C to be the smallest eigenvalue that can be represented by the
    # circuit.
    C = 2*math.pi / (2**register_size * t)  #What is a good value? Did this value work?
    print('C: ', C)
    # Simulate circuit
    #print("Expected observable outputs:")
    #print("X =", expected[0])
    #print("Y =", expected[1])
    #print("Z =", expected[2])
    #print("Actual: ")
    simulate(hhl_circuit(A, C, t, register_size, memory_size))


if __name__ == '__main__':
    main()
