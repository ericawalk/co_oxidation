import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 16})

energies_kCal_mol = np.array([0, 4.753065, 13.068390, 25.626708, 17.980683, 14.873617, 11.173369])
energies_eV = energies_kCal_mol*4.3363e-2
fig,ax = plt.subplots(figsize=(8,4))
ax.plot(range(7), energies_eV, color='tab:blue', linewidth=4)
ax.set_ylabel('Relative energy (eV)')
ax.set_ylim([0.0, 1.6])
fig.tight_layout()
fig.savefig('GSM_fig.png',dpi=220)
