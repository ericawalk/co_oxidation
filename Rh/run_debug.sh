#!/bin/sh
##SBATCH --constraint=UBHPC&CPU-Gold-6130&INTEL&u25&OPA&MRI #request 'skylake' nodes
##SBATCH --constraint=MRI|NIH #request 'cascade' and 'skylake' nodes
#SBATCH --constraint=CPU-Gold-6130
## alternate: #SBATCH --constraint=V100
#SBATCH --partition=debug
#SBATCH --qos=debug
#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1 #16
##SBATCH --constraint=IB
#SBATCH --mem=187000
##SBATCH --mem=23000
#SBATCH --job-name=set_pressures
#SBATCH --output=job.out
#SBATCH --mail-user=ericwalk@buffalo.edu
#SBATCH --mail-type=ALL
##SBATCH --requeue
#Specifies that the job will be requeued after a node failure.
#The default is that the job will not be requeued.

python visualization_full_matrix.py > quantum_out_4_qbit_set_pressures.txt
