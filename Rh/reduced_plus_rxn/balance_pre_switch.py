#!/usr/bin/env python
# coding: utf-8

# In[1]:


##HHL validate with the paper###

from qiskit import Aer, transpile, assemble
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.quantum_info import state_fidelity
from qiskit.aqua.algorithms import HHL, NumPyLSsolver
from qiskit.aqua.components.eigs import EigsQPE
from qiskit.aqua.components.reciprocals import LookupRotation
from qiskit.aqua.operators import MatrixOperator
from qiskit.aqua.components.initial_states import Custom
import numpy as np
from scipy.linalg import block_diag
# import matplotlib.pyplot as plt
# get_ipython().run_line_magic('matplotlib', 'inline')

p_CO = 0.4
p_O2 = 0.2
p_CO2 = 0.4

def create_eigs(matrix, num_auxiliary, num_time_slices, negative_evals):
    ne_qfts = [None, None]
    if negative_evals:
        num_auxiliary += 1
        ne_qfts = [QFT(num_auxiliary - 1), QFT(num_auxiliary - 1).inverse()]

    return EigsQPE(MatrixOperator(matrix=matrix),
                   QFT(num_auxiliary).inverse(),
                   num_time_slices=num_time_slices,
                   num_ancillae=num_auxiliary,
                   expansion_mode='suzuki',
                   expansion_order=2,
                   evo_time=None,  # This is t, can set to: np.pi*3/4
                   negative_evals=negative_evals,
                   ne_qfts=ne_qfts)


# In[2]:


def fidelity(hhl, ref):
    solution_hhl_normed = hhl / np.linalg.norm(hhl)
    solution_ref_normed = ref / np.linalg.norm(ref)
    fidelity = state_fidelity(solution_hhl_normed, solution_ref_normed)
    return fidelity

def one_block(T = 373.15, p_CO = p_CO, p_O2 = p_O2, p_CO2 = p_CO2, dG_1 = -0.807, dG_2 = -2.09, dGbar_3 = 1.09, dGbar_minus_3 = 1.60):




    kB = 8.617e-5
    h = 4.13566e-15
    k_1 = 3.8719294e6
    k_2 = 5.1231558e6
    k_minus_1 = k_1/np.exp(-(dG_1)/(kB*T))
    k_minus_2 = k_2/np.exp(-(dG_2)/(kB*T))
    k_3 = kB*T/h*np.exp(-(dGbar_3)/(kB*T))
    k_minus_3 = kB*T/h*np.exp(-(dGbar_minus_3)/(kB*T))

    theta_O = 0.5
    theta_CO = 0.5
    M_alg = np.array([[1, 1],
             [-k_minus_2 - k_3*theta_CO, k_minus_1 + k_3*theta_O]])

    #M_alg[1,1] = 1  #Same as multiplying on the diagonal by the reciprocal
    #M_alg[2,2] = 1

    M_alg[1,:] = M_alg[1,:]/max(np.absolute(M_alg[1,:])) #b vector is zero for these rows

    M_alg = np.matmul(np.array([[1/np.sqrt(2), 1/np.sqrt(2)], [1/np.sqrt(2), -1/np.sqrt(2)]]),M_alg)

    # x is theta_O, theta_CO
    #M_squiggle = np.zeros((8,8))
    #M_squiggle[0:4,4:8] = M_alg
    #M_squiggle[4:8,0:4] = np.transpose(M_alg)
    b = np.zeros((2,1)) #8
    b[0,0] = 1
    b = np.matmul(np.array([[1/np.sqrt(2), 1/np.sqrt(2)], [1/np.sqrt(2), -1/np.sqrt(2)]]),b)
    return(M_alg, b)
# In[3]:


#https://www.sciencedirect.com/science/article/pii/S037596012030462X?via%3Dihub
# to solve for linear equation Ax=b and comparing results with the paper

mean_block, mean_b = one_block()
up_95_block, up_95_b = one_block(dG_1 = -0.807+2*np.sqrt(4.15e-2), \
                                 dG_2 = -2.09+2*np.sqrt(3.79e-2), \
                                 dGbar_3 = 1.09+2*np.sqrt(8.89e-3), \
                                 dGbar_minus_3 = 1.60+2*np.sqrt(9.54e-2))
low_95_block, low_95_b = one_block(dG_1 = -0.807-2*np.sqrt(4.15e-2), \
                                 dG_2 = -2.09-2*np.sqrt(3.79e-2), \
                                 dGbar_3 = 1.09-2*np.sqrt(8.89e-3), \
                                 dGbar_minus_3 = 1.60-2*np.sqrt(9.54e-2))
b_vert = np.vstack((mean_b, up_95_b, low_95_b))
#b_fin = b_stack/np.sqrt(np.sum(b_stack)) #only holds when b is 1's and 0's
matrix = block_diag(up_95_block, mean_block, low_95_block)
#print(b_fin.shape)
#print(A_fin.shape)
print(matrix)
print(b_vert)

sol = np.matmul(np.linalg.inv(matrix),b_vert)
print(sol)

vector = b_vert.flatten()


# In[ ]:

# Matrix is already hermitian so this step might be cut in future versions.
orig_size = len(vector)

e,v=np.linalg.eig(matrix)

print('Matrix:\n',matrix)
print('Eigen Values:',e)
print('Condition Number:',max(e)/min(e))
#to find x using the HHL 


# In[4]:


orig_size = len(vector)
matrix, vector, truncate_powerdim, truncate_hermitian = HHL.matrix_resize(matrix, vector)

# Initialize eigenvalue finding module
eigs = create_eigs(matrix, 3, 50, False)
num_q, num_a = eigs.get_register_sizes()

# Initialize initial state module
init_state = Custom(num_q, state_vector=vector)

# Initialize reciprocal rotation module
reci = LookupRotation(negative_evals=eigs._negative_evals, evo_time=eigs._evo_time)

algo = HHL(matrix, vector, truncate_powerdim, truncate_hermitian, eigs,
           init_state, reci, num_q, num_a, orig_size)


circ=algo.construct_circuit(measurement=True)
#circ.draw(fold=-1)


# In[5]:


result = algo.run(QuantumInstance(Aer.get_backend('statevector_simulator')))
print("Solution:\t\t", np.round(result['solution'], 5))

#print(result)

result_ref = NumPyLSsolver(matrix, vector).run()
print("Classical Solution:\t", np.round(result_ref['solution'], 5))

print("Probability:\t\t %f" % result['probability_result'])
fid=fidelity(result['solution'], result_ref['solution'])
print("Fidelity:\t\t %f\n" % fid)


# In[6]:


#def get_hhl_fidelity(matrix,vector,condition_num):
#    orig_size = len(vector)
#    matrix, vector, truncate_powerdim, truncate_hermitian = HHL.matrix_resize(matrix, vector)

    # Initialize eigenvalue finding module
#    eigs = create_eigs(matrix, 3, 50, False)
#    num_q, num_a = eigs.get_register_sizes()

    # Initialize initial state module
#    init_state = Custom(num_q, state_vector=vector)

    # Initialize reciprocal rotation module
#    reci = LookupRotation(negative_evals=eigs._negative_evals, evo_time=eigs._evo_time)

#    algo = HHL(matrix, vector, truncate_powerdim, truncate_hermitian, eigs,
#           init_state, reci, num_q, num_a, orig_size)


#    circ=algo.construct_circuit(measurement=True)
    
#    result = algo.run(QuantumInstance(Aer.get_backend('statevector_simulator')))
    
#    print('Matrix:\n',matrix)
#    print('Condition Number:',condition_num)
#    print("Solution:\t\t", np.round(result['solution'], 5))

    #print(result)

#    result_ref = NumPyLSsolver(matrix, vector).run()
#    print("Classical Solution:\t", np.round(result_ref['solution'], 5))

#    print("Probability:\t\t %f" % result['probability_result'])
#    fid=fidelity(result['solution'], result_ref['solution'])
    
#    print("Fidelity:\t\t %f\n" % fid)
#    return fid
    


# In[7]:


#matlist=[np.array([[1,0],[0,1]]),
#         np.array([[1,0.2],[0.2,1]]),
#        np.array([[1,0],[0,0.5]]),
#        np.array([[1.5,0],[0,0.5]]),
#        np.array([[0.69,0.49],[0.49,0.92]]),
#        np.array([[0.75,0.5],[0.5,0.75]]),
#        np.array([[1,0],[0,0.1]]),
#        np.array([[1,0],[0,0.01]]),
#        np.array([[1,0],[0,0.001]]),
#        np.array([[1,1],[-1,1]])]

#result=np.zeros((len(matlist),2))

#for i in range(len(matlist)):
#    vector = [0.707, 0.707]
    
#    e,v=np.linalg.eig(matlist[i])
#    k=max(e)/min(e)

#    fid=get_hhl_fidelity(matlist[i],vector,k)
    
#    result[i,0]=round(k,3)
#    result[i,1]=round(fid,4)

#print('Fidelity for different condition number - HHL:')
#print(result)


#fig = plt.figure(figsize=(9,6),dpi=75)
#ax = plt.axes()

#ax.plot(result[:,0], result[:,1],color='blue');
#plt.title("Fidelity v/s Condition Number")
#plt.xlabel("Condition Number k")
#plt.ylabel("Fidelity");

#plt.savefig('2_2Matrix.png')
#plt.show()


# In[ ]:


# matlist=[np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]]),
#          np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,0.5]]),
#          np.array([[1,0.333,0,0],[0.333,1,0.333,0],[0,0.333,1,0.333],[0,0,0.333,1]]),
#          np.array([[0.97,0,0,0],[0,0.81,0,0],[0,0,0.3,0],[0,0,0,0.13]]),
#          np.array([[1,-0.5,0,0],[-0.5,1,-0.5,0],[0,-0.5,1,-0.5],[0,0,-0.5,1]]),
#          np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,0.1]]),
#          np.array([[0.99,0.37,0.31,0],[0.37,0.32,0.15,0.15],[0.31,0.15,0.6,0.21],[0,0.15,0.21,0.73]]),
#          np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,0.01]]),
#          np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,0.001]])]

# result=np.zeros((len(matlist),2))

# for i in range(len(matlist)):
#     vector = [0.707, 0.707, 0.707, 0.707]
    
#     e,v=np.linalg.eig(matlist[i])
#     k=max(e)/min(e)

#     fid=get_hhl_fidelity(matlist[i],vector,k)
    
#     result[i,0]=round(k,3)
#     result[i,1]=round(fid,4)

# print('Fidelity for different condition number - HHL:')
# print(result)

# fig = plt.figure(figsize=(9,6),dpi=75)
# ax = plt.axes()

# ax.plot(result[:,0], result[:,1],color='green');
# plt.title("Fidelity v/s Condition Number")
# plt.xlabel("Condition Number k")
# plt.ylabel("Fidelity");

# plt.savefig('2_2Matrix.png')
# plt.show()


# In[ ]:




