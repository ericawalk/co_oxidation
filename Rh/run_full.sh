#!/bin/sh
##SBATCH --constraint=UBHPC&CPU-Gold-6130&INTEL&u25&OPA&MRI #request 'skylake' nodes
#SBATCH --constraint=UBHPC&CPU-Gold-6130&INTEL&u25&OPA&MRI #request 'skylake' nodes
#SBATCH --constraint=MRI|NIH #request 'cascade' and 'skylake' nodes
#SBATCH --time=72:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
##SBATCH --constraint=IB
#SBATCH --mem=128000
##SBATCH --mem=23000
#SBATCH --job-name=fidelity_adapt
#SBATCH --output=job.out
#SBATCH --mail-user=ericwalk@buffalo.edu
#SBATCH --mail-type=ALL
##SBATCH --requeue
#Specifies that the job will be requeued after a node failure.
#The default is that the job will not be requeued.

python fidelity_full_matrix.py > fidelity_out_long_time.txt
