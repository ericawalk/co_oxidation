#Eric A. Walker
#This script reads DFT calculation results and produces the Gibbs free energy covariance matrix.
import matplotlib
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 20})
from matplotlib.ticker import FormatStrFormatter
import numpy as np
import extract_egy
from extract_egy import extract_egy
import extract_egy_ensemble
from extract_egy_ensemble import extract_egy_ensemble
import free_egy_correct
from free_egy_correct import free_egy_correct
import NIST_entropy_calc
from NIST_entropy_calc import NIST_entropy_calc
######################## Obtain free energies of DFT calculations:
T = 373.15
dict_ads = {
'CO': ['./CO'],
'O': ['./O'],
'CO_O_react': ['./C_O_bond_formation_double_ended_fourth_restart/scratch/vasp0001.01'],
'CO_O_prod': ['./C_O_bond_formation_double_ended_fourth_restart/scratch/vasp0001.11'],
'CO_O_TS': ['./C_O_bond_formation_double_ended_fourth_restart/scratch/vasp0001.04'],
'CO2': ['./CO2'],
'Pt111': ['./Pt111']
}

dict_gas = {
'CO': ['./CO_gas'],
'O2': ['./O2_gas'],
}

for key in dict_ads:
    if key == 'Pt111':
        free_energy = np.asarray(extract_egy(dict_ads[key]))
        free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads[key]))
        dict_ads[key].extend([free_energy,free_energy_ensemble])
    elif key == 'CO_O_react':
        CO2_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads[key]))
        CO2_energy = np.asarray(extract_egy(dict_ads[key]))
        CO2_ZPE = free_egy_correct(dict_ads[key], ads_or_gas = 'gas')
        CO2_entropy = NIST_entropy_calc(T = T, A = 24.99735, B = 55.18696, C = -33.69137, D = 7.948387, E = -0.136638, F = -403.6075, G = 228.2431)
        CO2_free_energy = CO2_energy + CO2_ZPE - CO2_entropy
        dict_ads['CO_O_react'].extend([CO2_free_energy, CO2_free_energy_ensemble])
    else:
        energy = np.asarray(extract_egy(dict_ads[key]))
        free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_ads[key]))
        free_energy = energy + free_egy_correct(dict_ads[key], T=T)
        dict_ads[key].extend([free_energy, free_energy_ensemble])

O2_gas_energy = np.asarray(extract_egy(dict_gas['O2']))
O2_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['O2']))
O2_gas_ZPE = free_egy_correct(dict_gas['O2'], ads_or_gas = 'gas')
O2_gas_entropy = NIST_entropy_calc(T = T, A = 30.03235, B = 8.772972, C = -3.988133, D = 0.788313, E = -0.741599, F = -11.32468, G = 236.1663)
O2_gas_free_energy = O2_gas_energy + O2_gas_ZPE - O2_gas_entropy
dict_gas['O2'].extend([O2_gas_free_energy, O2_gas_free_energy_ensemble])

CO_gas_energy = np.asarray(extract_egy(dict_gas['CO']))
CO_gas_free_energy_ensemble = np.asarray(extract_egy_ensemble(dict_gas['CO']))
CO_gas_ZPE = free_egy_correct(dict_gas['CO'], ads_or_gas = 'gas')
CO_gas_entropy = NIST_entropy_calc(T = T, A = 25.56759, B = 6.096130, C = 4.054656, D = -2.671301, E = 0.131021, F = -118.0089, G = 227.3665)
CO_gas_free_energy = CO_gas_energy + CO_gas_ZPE - CO_gas_entropy
dict_gas['CO'].extend([CO_gas_free_energy, CO_gas_free_energy_ensemble])

print(dict_ads['CO'][1],dict_ads['Pt111'][1],dict_gas['CO'][1])
delta_G_1 = dict_ads['CO'][1] - dict_ads['Pt111'][1] - dict_gas['CO'][1]
delta_G_2 = dict_ads['O'][1] - dict_ads['Pt111'][1] - 0.5*dict_gas['O2'][1]
delta_G_minus_3_act_bar = dict_ads['CO_O_TS'][1] - dict_ads['CO_O_react'][1]
delta_G_3_act_bar = dict_ads['CO_O_TS'][1] - dict_ads['CO_O_prod'][1]

delta_G_1_ensemble = dict_ads['CO'][2] - dict_ads['Pt111'][2] - dict_gas['CO'][2]
delta_G_2_ensemble = dict_ads['O'][2] - dict_ads['Pt111'][2] - 0.5*dict_gas['O2'][2]
delta_G_minus_3_act_bar_ensemble = dict_ads['CO_O_TS'][2] - dict_ads['CO_O_react'][2]
delta_G_3_act_bar_ensemble = dict_ads['CO_O_TS'][2] - dict_ads['CO_O_prod'][2]

print('mu delta_G_1', delta_G_1, '\n')
print('mu delta_G_2', delta_G_2, '\n')
print('mu delta_G_3_act_bar', delta_G_3_act_bar, '\n')
print('mu delta_G_minus_3_act_bar', delta_G_minus_3_act_bar, '\n')

G_stacked = np.stack((delta_G_1_ensemble, delta_G_2_ensemble, delta_G_3_act_bar_ensemble, delta_G_minus_3_act_bar_ensemble), axis = 0)  
G_cov = np.cov(G_stacked)

print('G_cov (eV) same order as mu',G_cov) 
