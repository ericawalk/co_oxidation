#Eric A. Walker
import numpy as np

ancilla = np.array([[1],[0]])
register_qubit_0 = np.array([[1],[0]])
register_qubit_1 = np.array([[1],[0]])
register_qubit_2 = np.array([[1],[0]])
register_qubit_3 = np.array([[1],[0]])
memory_qubit_0 = np.array([[1],[0]])
memory_qubit_1 = np.array([[1],[0]])
memory_qubit_2 = np.array([[1],[0]])
register = np.kron(register_qubit_3,np.kron(register_qubit_2,np.kron(register_qubit_1,register_qubit_0)))
# register is now a 16x1 vector
memory = np.kron(memory_qubit_2,np.kron(memory_qubit_1,memory_qubit_0))

################### begin Quantun Phase Estimation
